<?php 
	include 'header.php';
?>


	<div class="l-page">
		<div class="l-content">

			<div class="page-bg">

				<div class="l-gutter l-gutter-med">

					<main class="l-main is-width-full">
						<div class="l-main-content">

							<?php 
								include 'main-menu.php';
							?>

							<article class="article">
								
								<div class="article-portrait">
									<img src="http://placehold.it/1154x480" />
								</div>

								<header class="article-header">
									<div class="article-header-h-outer">
										<h1 class="article-header-h">
											<a href="#">
												Lorem Ipsum Dolor Sit Amet
											</a>
										</h1>
									</div>
									<div class="article-header-meta">
										<p>
											<span>by <a href="#">John Doe</a></span> &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
											<span>Published April 24,2016</span>
										</p>
									</div>
								</header>

								<div class="article-story">
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>
									<p>
										Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
									</p>
									<p>
										At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.
									</p>
								</div>

								<div class="article-share">
									<ul class="">
										<li class="">
											<a href="#"><i class="sprite sprite-ico-social-f2"></i></a></li>
										<li>
											<a href="#"><i class="sprite sprite-ico-social-p2"></i></a></li>
										<li>
											<a href="#"><i class="sprite sprite-ico-social-t2"></i></a></li>
										<li>
											<a href="#"><i class="sprite sprite-ico-social-b2"></i></a></li>
										<li>
											<a href="#"><i class="sprite sprite-ico-social-tb2"></i></a></li>
										<li>
											<a href="#"><i class="sprite sprite-ico-social-m2"></i></a></li>
									</ul>
								</div>

								<div class="article-comment">

									<form class="article-comment-frm">
										<header class="article-comment-frm-header">
											<div class="article-comment-frm-header-counter">
												<p>
													0 Comments
												</p>
											</div>

											<div class="article-comment-frm-header-sort">
												<div>
													<p>
														Sort By
													</p>
												</div>
												<div>
													<a href="#" class="sort-trigger">Oldest</a>
													<ul class="sort-submenu">
														<li><a href="#">Oldest</a></li>
														<li><a href="#">Newest</a></li>
													</ul>
												</div>
											</div>
										</header>
										<div class="article-comment-frm-inputs">
											<div class="article-comment-frm-inputs-userportrait">
												<img src="http://placehold.it/70x70" />
											</div>
											<div class="article-comment-frm-inputs-entry">
												<textarea placeholder="Write comments..."></textarea>
											</div>
											<div class="article-comment-frm-inputs-ctrl">
												<input type="button" value="SUBMIT" />
											</div>
										</div>
									</form>

								</div>

							</article>

						</div>
					</main>

					

					<div class="clear-both"></div>


				</div>


			</div>





			<div class="page-related-bg">
				<div class="l-gutter">
					<div class="recent-post">
						<header class="recent-post-header">
							<h3 class="recent-post-header-h">
								Recent Post
							</h3>
						</header>
						<div class="recent-post-cards">
							<ul class="l-grid l-grid-3cols">
								<li class="l-grid-col">
									<a href="detail.php" class="recent-post-cards-anc">
										<div class="recent-post-cards-portrait">
											<span class="recent-post-cards-portrait-ribbon">
												Category
											</span>
											<p class="recent-post-cards-portrait-title">
												Sed ut perspiciatis unde omnis iste
											</p>		
											<img src="http://placehold.it/400x283" />
										</div>
									</a>
								</li>
								<li class="l-grid-col">
									<a href="detail.php" class="recent-post-cards-anc">
										<div class="recent-post-cards-portrait">
											<span class="recent-post-cards-portrait-ribbon">
												Category
											</span>
											<p class="recent-post-cards-portrait-title">
												Sed ut perspiciatis unde omnis iste
											</p>		
											<img src="http://placehold.it/400x283" />
										</div>
									</a>
								</li>
								<li class="l-grid-col">
									<a href="detail.php" class="recent-post-cards-anc">
										<div class="recent-post-cards-portrait">
											<span class="recent-post-cards-portrait-ribbon">
												Category
											</span>
											<p class="recent-post-cards-portrait-title">
												Sed ut perspiciatis unde omnis iste
											</p>		
											<img src="http://placehold.it/400x283" />
										</div>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>




		</div>
	</div>



<?php 
	include 'footer.php';
?>