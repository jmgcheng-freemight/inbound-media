<?php 
	include 'header.php';
?>


	<div class="l-page">
		<div class="l-content">

			<div class="page-bg">

				<div class="l-gutter">

					<main class="l-main">
						<div class="l-main-content">

							<?php 
								include 'main-menu.php';
							?>
							
							<div class="article-cards">
								<ul>
									<li>
										<a href="detail.php" class="article-cards-anc">
											<div class="article-cards-portrait">
												<span class="article-cards-portrait-ribbon">
													Category
												</span>
												<p class="article-cards-portrait-title">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem
												</p>		
												<span class="article-cards-portrait-overlay">
													<img src="img/bg-3.png" />	
												</span>
												<img src="http://placehold.it/834x413" />
											</div>
										</a>
									</li>
									<li>
										<a href="detail.php" class="article-cards-anc">
											<div class="article-cards-portrait">
												<span class="article-cards-portrait-ribbon">
													Category
												</span>
												<p class="article-cards-portrait-title">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem
												</p>		
												<span class="article-cards-portrait-overlay">
													<img src="img/bg-3.png" />	
												</span>
												<img src="http://placehold.it/834x413" />
											</div>
										</a>
									</li>
									<li>
										<a href="detail.php" class="article-cards-anc">
											<div class="article-cards-portrait">
												<span class="article-cards-portrait-ribbon">
													Category
												</span>
												<p class="article-cards-portrait-title">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem
												</p>		
												<span class="article-cards-portrait-overlay">
													<img src="img/bg-3.png" />	
												</span>
												<img src="http://placehold.it/834x413" />
											</div>
										</a>
									</li>
									<li>
										<a href="detail.php" class="article-cards-anc">
											<div class="article-cards-portrait">
												<span class="article-cards-portrait-ribbon">
													Category
												</span>
												<p class="article-cards-portrait-title">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem
												</p>		
												<span class="article-cards-portrait-overlay">
													<img src="img/bg-3.png" />	
												</span>
												<img src="http://placehold.it/834x413" />
											</div>
										</a>
									</li>
									<li>
										<a href="detail.php" class="article-cards-anc">
											<div class="article-cards-portrait">
												<span class="article-cards-portrait-ribbon">
													Category
												</span>
												<p class="article-cards-portrait-title">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem
												</p>		
												<span class="article-cards-portrait-overlay">
													<img src="img/bg-3.png" />	
												</span>
												<img src="http://placehold.it/834x413" />
											</div>
										</a>
									</li>
								</ul>

								<div class="loadmore">
									<div style="display:none; background:#de3822">de3822</div>
									<div style="display:none; background:#e6402a">e6402a</div>
									<a href="#" class="button width-full">
										More
									</a>
								</div>

							</div>




							
						</div>
					</main>

					<?php include 'sidebar.php'; ?>

					<div class="clear-both"></div>


				</div>


			</div>

		</div>
	</div>



<?php 
	include 'footer.php';
?>